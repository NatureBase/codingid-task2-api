﻿using BookSalesAPI.Data;
using BookSalesAPI.DTos;
using BookSalesAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BookSalesAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BookController : ControllerBase
    {
        private readonly BookData _bookData; 
        public BookController(BookData bookData) 
        {
            _bookData = bookData;
        }

        [HttpGet("GetAll")]
        public IActionResult GetAll() 
        {
            try
            {
                List<Book> books = _bookData.GetAll();

                return Ok(books);
            }
            catch (Exception ex) 
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetById/{id}")]

        public IActionResult GetById(Guid id) 
        {
            try 
            {
                Book? book = _bookData.GetById(id);

                if (book == null) 
                {
                    return NotFound($"Data {id} Not Found");
                }

                return Ok(book);
            }

            catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost]

        public IActionResult Post([FromBody] BookDTo bookDTo) 
        {
            try
            {
                Book book = new Book
                {
                    Id = Guid.NewGuid(),
                    Title = bookDTo.Title,
                    Description = bookDTo.Description,
                    Author = bookDTo.Author,
                    Stock = bookDTo.Stock,
                    Created = bookDTo.Created,
                    Updated = bookDTo.Updated

                };

                bool result = _bookData.Insert(book);

                if (result) 
                {
                    return StatusCode(201, book.Id);
                }
                else 
                {
                    return StatusCode(500, "Data Not Inserted");
                }
            }
            catch (Exception ex) 
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
