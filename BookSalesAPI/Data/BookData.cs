﻿using System.Data.SqlClient;
using BookSalesAPI.Models;
using static System.Reflection.Metadata.BlobBuilder;

namespace BookSalesAPI.Data
{
    public class BookData
    {
        private readonly string ConnectionString;
        private readonly IConfiguration _configuration;

        public BookData(IConfiguration configuration) 
        {
            _configuration = configuration;
            ConnectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        public List<Book> GetAll() 
        { 
            List<Book> books = new List<Book>();

            string query = "SELECT * FROM Books";

            using(SqlConnection connection = new SqlConnection(ConnectionString)) 
            {
                using (SqlCommand command = new SqlCommand(query, connection)) 
                {
                    try 
                    {
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                books.Add(new Book
                                {
                                    Id = Guid.Parse(reader["Id"].ToString() ?? string.Empty),
                                    Title = reader["Title"].ToString() ?? string.Empty,
                                    Description = reader["Description"].ToString() ?? string.Empty,
                                    Author = reader["Author"].ToString() ?? string.Empty,
                                    Stock = Convert.ToInt32(reader["Stock"]),
                                    Created = Convert.ToDateTime(reader["Created"]),
                                    Updated = Convert.ToDateTime(reader["Updated"]),
                                });

                            }
                        }
                    }

                    catch 
                    {
                        throw;
                    }

                    finally 
                    {
                        connection.Close();
                    }
                    
                }
            }

            return books;
        }

        public Book? GetById(Guid Id) 
        {
            Book? book = null;
            
            string query = $"SELECT * FROM Books WHERE Id = '{Id}'";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                book = new Book
                                {
                                    Id = Guid.Parse(reader["Id"].ToString() ?? string.Empty),
                                    Title = reader["Title"].ToString() ?? string.Empty,
                                    Description = reader["Description"].ToString() ?? string.Empty,
                                    Author = reader["Author"].ToString() ?? string.Empty,
                                    Stock = Convert.ToInt32(reader["Stock"]),
                                    Created = Convert.ToDateTime(reader["Created"]),
                                    Updated = Convert.ToDateTime(reader["Updated"]),
                                };

                            }
                        }
                    }

                    catch
                    {
                        throw;
                    }

                    finally
                    {
                        connection.Close();
                    }

                }
            }

            return book;
        }

        public bool Insert(Book book) 
        {
            bool result = false;

            string query = $"INSERT INTO Books (Id, Title, Description, Author, Stock, Created, Updated) VALUES (" +
                $"'{book.Id}','{book.Title}','{book.Description}','{book.Author}','{book.Stock}','{book.Created}','{book.Updated}')";

            using (SqlConnection connection = new SqlConnection(ConnectionString)) 
            {
                using (SqlCommand command = new SqlCommand()) 
                {
                    command.Connection = connection;
                    command.CommandText = query;

                    try
                    {
                        connection.Open() ;

                        result = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                    catch
                    {
                        throw;
                    }
                    finally 
                    {
                        connection.Close();
                    }
                }
            }

            return result;
        }

	}
}
